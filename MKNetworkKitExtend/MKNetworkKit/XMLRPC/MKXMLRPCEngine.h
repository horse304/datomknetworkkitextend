//
//  MKXMLRPCEngine.h
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

@interface MKXMLRPCEngine : MKNetworkEngine
/*!
 *  @abstract Post 1 request theo chuan XML RPC toi server
 *  
 *  @discussion
 *	Tao 1 operation voi duong dan URL truyen vao
 *  Convert methodName va params truyen vao -> XML theo chuan XML RPC de gui len server.
 *  HTTP Method mac dinh la post
 *  Text encoding mac dinh la text/xml
 *  
 */
-(MKXMLRPCOperation *) operationWithPath:(NSString*) path 
                              callMethod:(NSString *)methodName 
                              withParams:(NSMutableArray *)params;

-(MKXMLRPCOperation *) operationWithPath:(NSString*) path 
                              callMethod:(NSString *)methodName 
                              withParams:(NSMutableArray *)params 
                                     ssl:(BOOL) useSSL;
@end
