//
//  MKXMLRPCEngine.m
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKXMLRPCEngine.h"

@implementation MKXMLRPCEngine

-(MKXMLRPCOperation *)operationWithPath:(NSString *)path 
                              callMethod:(NSString *)methodName 
                              withParams:(NSMutableArray *)params{
    return [self operationWithPath:path 
                        callMethod:methodName 
                        withParams:params 
                               ssl:NO];
}

-(MKXMLRPCOperation *) operationWithPath:(NSString*) path 
                              callMethod:(NSString *)methodName 
                              withParams:(NSMutableArray *)params 
                                     ssl:(BOOL) useSSL{
    
    if(self.readonlyHostName == nil) {
        DLog(@"Hostname is nil, use operationWithURLString: method to create absolute URL operations");
        return nil;
    }
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@://%@", useSSL ? @"https" : @"http", self.readonlyHostName];
    
    if(self.portNumber != 0)
        [urlString appendFormat:@":%d", self.portNumber];
    
    if(self.apiPath) 
        [urlString appendFormat:@"/%@", self.apiPath];
    
    [urlString appendFormat:@"/%@", path];
    
    MKXMLRPCOperation *op = [[MKXMLRPCOperation alloc] initWithURLString:urlString 
                                                              callMethod:methodName 
                                                                  params:params];
    return op;
}
@end
