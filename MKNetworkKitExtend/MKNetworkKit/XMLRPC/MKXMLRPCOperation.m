//
//  MKXMLRPCOperation.m
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKXMLRPCOperation.h"
#define PARAMS_METHOD_NAME_KEY @"methodName"
#define PARAMS_METHOD_PARAMS_KEY @"methodParams"
#define XMLRPC_TYPE_ENCODING @"text/xml"

@implementation MKXMLRPCOperation
@synthesize             responseXMLRPC = _responseXMLRPC;

- (id)initWithURLString:(NSString *)aURLString 
             callMethod:(NSString *)methodName 
                 params:(NSMutableArray *)params{
    
    NSMutableDictionary *xmlrpcParams = [[NSMutableDictionary alloc] init ];
    [xmlrpcParams setValue:methodName forKey:PARAMS_METHOD_NAME_KEY];
    [xmlrpcParams setValue:params forKey:PARAMS_METHOD_PARAMS_KEY];
    
    self = [super initWithURLString:aURLString params:xmlrpcParams httpMethod:@"POST"];
    
    if (self) {
        __block MKXMLRPCOperation *xmlOperation = self;
        
        //Set XMLRPC PostData Encoding
        [self setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
            return [xmlOperation encode:(NSDictionary *)postDataDict];
        } forType:XMLRPC_TYPE_ENCODING];
        
        //Set onCompletion handler
        [self onCompletion:^(MKNetworkOperation *completedOperation) {
            MKXMLRPCOperation *xmlCompletedOperation = (MKXMLRPCOperation *)completedOperation;
            [xmlCompletedOperation setResponseXMLRPC:[xmlCompletedOperation decode:xmlCompletedOperation.responseData]];
        } onError:^(NSError *error) {
            DLog(@"%@",error);
        }];
    }
    return self;
}

#pragma mark - Encode params to XML String functions
- (NSString *)encode:(NSDictionary *)postDataDic{
    NSString *methodName = [postDataDic valueForKey:PARAMS_METHOD_NAME_KEY];
    NSMutableArray * params = [postDataDic valueForKey:PARAMS_METHOD_PARAMS_KEY];
    
    NSString *paramsString = @"";
    for (id object in params) {        
        paramsString = [paramsString stringByAppendingFormat:@"<param><value>%@</value></param>",[self encodeObject:object]];
    }
    
    DLog(@"%@",paramsString);
    NSString *header_xmlrpc = @"<?xml version=\"1.0\"?>";
    NSString *body_xmlrpc = [NSString stringWithFormat:@"<methodCall><methodName>%@</methodName><params>%@</params></methodCall>",methodName, paramsString];
    DLog(@"%@",[NSString stringWithFormat:@"%@%@",header_xmlrpc, body_xmlrpc]);
    return [NSString stringWithFormat:@"%@%@",header_xmlrpc, body_xmlrpc];
}

- (NSString *)encodeObject:(id)object{
    if (object == nil) {
        return nil;
    }
    
    if ([object isKindOfClass:[NSNumber class]]) {
        return [self encodeNumber:object];
    }
    else if([object isKindOfClass:[NSString class]]){
        return [self encodeString:object];
    }
    else if([object isKindOfClass:[NSDate class]]){
        return [self encodeDate:object];
    }
    else if ([object isKindOfClass:[NSData class]]) {
        return [self encodeDataBase64:object];
    }
    else if([object isKindOfClass:[NSArray class]]){
        return [self encodeArray:object];
    }else if([object isKindOfClass:[NSDictionary class]]){
        return [self encodeDictionary:object];
    }
    
    return nil;
}

- (NSString *)encodeDictionary:(NSDictionary *)dicParam{
    NSString *membersString = @"";
    for (NSString * key in [dicParam allKeys]) {
        id value = [dicParam valueForKey:key];
        if (value != nil) {
            membersString = [membersString stringByAppendingFormat:@"<member><name>%@</name><value>%@</value></member>",key,[self encodeObject:value]];
        }
    }
    return [NSString stringWithFormat:@"<struct>%@</struct>", membersString];
}

- (NSString *)encodeArray:(NSArray *)arrayParam{
    NSString *itemsString = @"";
    for (id item in arrayParam) {
        if (item != nil) {
            itemsString = [itemsString stringByAppendingFormat:@"<value>%@</value>",[self encodeObject:item]];
        }
    }
    return [NSString stringWithFormat:@"<array><data>%@</data></array>", itemsString];
}

- (NSString *)encodeNumber:(NSNumber *)numberParam{
    if (NSNUMBER_IS_BOOLEAN(numberParam)) {
        return [NSString stringWithFormat:@"<boolean>%d</boolean>",[numberParam boolValue]];
    }else if(NSNUMBER_IS_INTERGER(numberParam)){
        return [NSString stringWithFormat:@"<i4>%d</i4>",[numberParam intValue]];
    }else if (NSNUMBER_IS_DOUBLE(numberParam)
              || NSNUMBER_IS_FLOAT(numberParam)) {
        return [NSString stringWithFormat:@"<double>%f</double>",[numberParam doubleValue]];
    }
    return @"";
}

- (NSString *)encodeString:(NSString *)stringParam{
    return [NSString stringWithFormat:@"<string>%@</string>",stringParam];
}

- (NSString *)encodeDate:(NSDate *)dateParam{
    unsigned components = kCFCalendarUnitYear | kCFCalendarUnitMonth | kCFCalendarUnitDay | kCFCalendarUnitHour | kCFCalendarUnitMinute | kCFCalendarUnitSecond;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components: components fromDate: dateParam];
    NSString *buffer = [NSString stringWithFormat: @"%.4d%.2d%.2dT%.2d:%.2d:%.2d", [dateComponents year], [dateComponents month], [dateComponents day], [dateComponents hour], [dateComponents minute], [dateComponents second], nil];
    return [NSString stringWithFormat:@"<dateTime.iso8601>%@</dateTime.iso8601>", buffer];
}

- (NSString *)encodeDataBase64:(NSData *)dataParam{
    return [dataParam base64EncodedString];
}

#pragma mark - Decode functions
- (NSDictionary *)decode:(NSData *)responseData{
    DatoXMLParser * xmlParser = [[DatoXMLParser alloc] initWithData:responseData delegate:self];
    [xmlParser start];
    
    DatoXMLNode * xmlMethodResponseNode = xmlParser.xmlDOM;
    DatoXMLNode * childNodeMethodResponse = [xmlMethodResponseNode.childNodes objectAtIndex:0];
    if ([childNodeMethodResponse.nodeName.lowercaseString isEqualToString:@"fault"]) {
        id faultDecoded = [self decodeFaultXML:childNodeMethodResponse];
        
        NSMutableDictionary * responseFault = [[NSMutableDictionary alloc] init ];
        [responseFault setValue:[NSNumber numberWithBool:FALSE] forKey:RESPONSE_XMLRPC_ISSUCCESS_KEY];
        
        [responseFault setValue:faultDecoded forKey:RESPONSE_XMLRPC_FAULT_KEY];
        return responseFault;
    }else if([childNodeMethodResponse.nodeName.lowercaseString isEqualToString:@"params"]){
        NSArray * paramsDecoded = [self decodeParamsXML:childNodeMethodResponse];
        
        NSMutableDictionary * responseFault = [[NSMutableDictionary alloc] init ];
        [responseFault setValue:[NSNumber numberWithBool:TRUE] forKey:RESPONSE_XMLRPC_ISSUCCESS_KEY];
        
        [responseFault setValue:paramsDecoded forKey:RESPONSE_XMLRPC_PARAMS_KEY];
        return responseFault;
    }
    
    return nil;
}

- (id)decodeFaultXML:(DatoXMLNode *)faultXMLNode{
    DatoXMLNode * valueNode = [[faultXMLNode getElementsByTagName:@"value"] objectAtIndex:0];
    
    return [self decodeObject:[valueNode.childNodes objectAtIndex:0]];
}

- (NSArray *)decodeParamsXML:(DatoXMLNode *)paramsXMLNode{
    NSMutableArray * paramArray = [[NSMutableArray alloc] init ];
    NSArray *paramXMLNodeArray = [paramsXMLNode getElementsByTagName:@"param"];
    for (DatoXMLNode *paramNode in paramXMLNodeArray) {
        DatoXMLNode * valueNode = [[paramNode getElementsByTagName:@"value"] objectAtIndex:0];
        
        id objectDecoded = [self decodeObject:[valueNode.childNodes objectAtIndex:0]];
        if (objectDecoded != nil) {
            [paramArray addObject:objectDecoded];
        }else {
            DLog(@"Cannot decode XML Node %@",paramNode);
        }
    }
    return [NSArray arrayWithArray:paramArray];
}

- (id)decodeObject:(DatoXMLNode *)objectXMLNode{
    NSString *nodeName = objectXMLNode.nodeName.lowercaseString;
    if ([nodeName isEqualToString:@"int"]
        || [nodeName isEqualToString:@"i4"]) {
        return [self decodeInt:objectXMLNode];
    }else if([nodeName isEqualToString:@"boolean"]){
        return [self decodeBoolean:objectXMLNode];
    }else if ([nodeName isEqualToString:@"string"]) {
        return [self decodeString:objectXMLNode];
    }else if ([nodeName isEqualToString:@"double"]) {
        return [self decodeDouble:objectXMLNode];
    }else if([nodeName isEqualToString:@"datetime.iso8601"]){
        return [self decodeDate:objectXMLNode];
    }else if ([nodeName isEqualToString:@"base64"]) {
        return [self decodeDataBase64:objectXMLNode];
    }else if ([nodeName isEqualToString:@"struct"]) {
        return [self decodeDictionary:objectXMLNode];
    }else if ([nodeName isEqualToString:@"array"]) {
        return [self decodeArray:objectXMLNode];
    }
    return nil;
}

- (NSNumber *)decodeInt:(DatoXMLNode *)numberXMLNode{
    @try {
        return [NSNumber numberWithInt:[numberXMLNode.nodeValue intValue]];
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
        return nil;
    }
}

- (NSNumber *)decodeBoolean:(DatoXMLNode *)booleanXMLNode{
    @try {
        return [NSNumber numberWithBool:[booleanXMLNode.nodeValue boolValue]];
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
        return nil;
    }
}

- (NSNumber *)decodeDouble:(DatoXMLNode *)doubleXMLNode{
    @try {
        return [NSNumber numberWithDouble:[doubleXMLNode.nodeValue doubleValue]];
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
        return nil;
    }
}
               
- (NSString *)decodeString:(DatoXMLNode *)stringXMLNode{
    @try {
        if (stringXMLNode.nodeValue == nil) {
            return @"";
        }else {
            return (NSString *)stringXMLNode.nodeValue;
        }
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
        return nil;
    }
}

- (NSDate *)decodeDate:(DatoXMLNode *)dateXMLNode{
    @try {
        NSDate *result = nil;
        
        result = [self parseDateString: dateXMLNode.nodeValue withFormat: @"yyyyMMdd'T'HH:mm:ss"];
        
        if (!result) {
            result = [self parseDateString: dateXMLNode.nodeValue withFormat: @"yyyy'-'MM'-'dd'T'HH:mm:ss"];
        }
        
        if (!result) {
            result = (NSDate *)[NSNull null];
        }
        
        return result;
    }
    @catch (NSException *exception) {
        DLog(@"%@",exception);
        return nil;
    }
}

- (NSDate *)parseDateString: (NSString *)dateString withFormat: (NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSDate *result = nil;
    
    [dateFormatter setDateFormat: format];
    
    result = [dateFormatter dateFromString: dateString];
    return result;
}

- (NSData *)decodeDataBase64:(DatoXMLNode *)dataXMLNode{
    return [NSData dataFromBase64String: dataXMLNode.nodeValue];
}

- (NSArray *)decodeArray:(DatoXMLNode *)arrayXMLNode{
    NSMutableArray * result = [[NSMutableArray alloc] init ];
    DatoXMLNode *dataXMLNode = [[arrayXMLNode getElementsByTagName:@"data"] objectAtIndex:0];
    NSArray *valueXMLArray = [dataXMLNode getElementsByTagName:@"value"];
    for (DatoXMLNode *valueXMLNode in valueXMLArray) {
        DatoXMLNode *objectXMLNode = [valueXMLNode.childNodes objectAtIndex:0];
        id objectDecoded = [self decodeObject:objectXMLNode];
        if (objectDecoded != nil) {
            [result addObject:objectDecoded];
        }else {
            DLog(@"Cannot decode XML Node:%@",objectXMLNode);
        }
    }
    return [NSArray arrayWithArray:result];
}

- (NSDictionary *)decodeDictionary:(DatoXMLNode *)dictXMLNode{
    NSMutableDictionary * result = [[NSMutableDictionary alloc] init ];
    NSArray *memberXMLArray = [dictXMLNode getElementsByTagName:@"member"];
    for (DatoXMLNode *memberXMLNode in memberXMLArray) {
        DatoXMLNode * nameOfMember = [[memberXMLNode getElementsByTagName:@"name"] objectAtIndex:0];
        DatoXMLNode * valueOfMember = [[memberXMLNode getElementsByTagName:@"value"] objectAtIndex:0];
        DatoXMLNode * objectXMLNode = [valueOfMember.childNodes objectAtIndex:0];
        id objectDecoded = [self decodeObject:objectXMLNode];
        if (objectDecoded != nil) {
            [result setValue:objectDecoded forKey:nameOfMember.nodeValue];
        }else {
            DLog(@"Cannot decode XML Node:%@",objectXMLNode);
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:result];
}
@end
