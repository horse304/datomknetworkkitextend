//
//  XMLDOM.h
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

//#import <Foundation/Foundation.h>

@interface DatoXMLNode : NSObject
@property (retain, nonatomic) id parentNode;
@property (retain, nonatomic) NSMutableArray *childNodes;
@property (retain, nonatomic) NSString *nodeName;
@property (retain, nonatomic) id nodeValue;
@property (retain, nonatomic) NSDictionary *attributes;

-(id)initWithNodeName:(NSString *)nodeName;
-(NSArray *)getElementsByTagName:(NSString *)tagName;
-(NSArray *)getElementsByValue:(NSString *)value OfAttribute:(NSString *)attribute;
-(void)appendChild:(DatoXMLNode *)node;
-(DatoXMLNode*)firstChild;
-(DatoXMLNode*)lastChild;
-(void)removeChild:(DatoXMLNode *)node;
@end
