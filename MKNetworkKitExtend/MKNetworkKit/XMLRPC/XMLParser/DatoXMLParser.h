//
//  XMLParser.h
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//
//#import <Foundation/Foundation.h>
//#import "DatoXMLNode.h"

@protocol DatoXMLParserDelegate<NSObject>
@optional
-(void)datoXMLParserDidFinish:(id)sender xmlDocument:(DatoXMLNode *)xmlDOM;
@end

@interface DatoXMLParser : NSObject<NSXMLParserDelegate>
@property (retain, nonatomic) id<DatoXMLParserDelegate> delegate;
@property (retain, nonatomic) NSData *data;
@property (retain, nonatomic) DatoXMLNode *xmlDOM;
@property (retain, nonatomic) NSString *identifier;

-(id)initWithData:(NSData *)data delegate:(id)delegate;
-(void)start;
@end
