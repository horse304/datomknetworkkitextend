//
//  XMLParser.m
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//
#import "DatoXMLParser.h"
@interface DatoXMLParser()
@property (retain, nonatomic) NSXMLParser *xmlparser;
@property (retain, nonatomic) NSMutableArray *workingEntry;
@property (retain, nonatomic) NSMutableString *workingString;
@property (retain, nonatomic) NSMutableData *workingData;
@end

@implementation DatoXMLParser
@synthesize data = _data;
@synthesize delegate = _delegate;
@synthesize xmlDOM = _rootNode;
@synthesize xmlparser = _xmlparser;
@synthesize workingEntry = _workingEntry;
@synthesize workingString = _workingString;
@synthesize workingData = _workingData;
@synthesize identifier = _identifier;

-(id)initWithData:(NSData *)data delegate:(id)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.data = data;
    }
    return self;
}

-(void)start{
    //Start parser data
    _xmlparser = [[NSXMLParser alloc] initWithData:self.data];
    self.xmlparser.delegate = self;
    [self.xmlparser parse];
}

#pragma mark NSXMLParserDelegate
- (void)parserDidStartDocument:(NSXMLParser *)parser{
    _workingEntry = [[NSMutableArray alloc] init];
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    DatoXMLNode *element = [[DatoXMLNode alloc] initWithNodeName:elementName];
    element.attributes = [attributeDict mutableCopy];
    _workingString = [[NSMutableString alloc] init ];
    _workingData = [[NSMutableData alloc] init ];
    
    //Add element to parent node
    if (self.workingEntry.lastObject != nil) {
        //Set parentNode
        [[self.workingEntry.lastObject childNodes] addObject:element];
        element.parentNode = self.workingEntry.lastObject;
        [self.workingEntry addObject:element];
    }else {
        //Root node
        [self.workingEntry addObject:element];
    }
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    [self.workingString appendString:string];
}
-(void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock{
    [self.workingData appendData:CDATABlock];
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    NSString *trimmedString = [self.workingString stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedString.length>0) {        
        [self.workingEntry.lastObject setNodeValue:trimmedString];
    }
    if (self.workingData.length>0) {
        [self.workingEntry.lastObject setNodeValue:self.workingData];
    }
    
    if (self.workingEntry.count > 1) {        
        [self.workingEntry removeObjectAtIndex:(self.workingEntry.count-1)];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    self.xmlDOM = self.workingEntry.lastObject;
    if ([self.delegate respondsToSelector:@selector(datoXMLParserDidFinish:xmlDocument:)]) {
        [self.delegate datoXMLParserDidFinish:self xmlDocument:self.xmlDOM];
    }
}
@end
