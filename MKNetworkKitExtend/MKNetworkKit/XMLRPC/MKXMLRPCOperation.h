//
//  MKXMLRPCOperation.h
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#define RESPONSE_XMLRPC_ISSUCCESS_KEY @"isSuccess"
#define RESPONSE_XMLRPC_FAULT_KEY @"fault"
#define RESPONSE_XMLRPC_PARAMS_KEY @"params"
#define NSNUMBER_IS_BOOLEAN(v) (strcmp([v objCType], @encode(BOOL)) == 0)
#define NSNUMBER_IS_INTERGER(v) (strcmp([v objCType], @encode(NSInteger)) == 0)
#define NSNUMBER_IS_DOUBLE(v) (strcmp([v objCType], @encode(double)) == 0)
#define NSNUMBER_IS_FLOAT(v) (strcmp([v objCType], @encode(float)) == 0)

@class MKXMLRPCOperation;

@interface MKXMLRPCOperation : MKNetworkOperation

@property (strong, nonatomic) NSDictionary * responseXMLRPC;

- (id)initWithURLString:(NSString *)aURLString 
             callMethod:(NSString *)methodName 
                 params:(NSMutableArray *)params;
@end
