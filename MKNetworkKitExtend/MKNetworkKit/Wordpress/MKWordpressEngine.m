//
//  MKWordpressEngine.m
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKWordpressEngine.h"
#define XMLRPC_PATH @"xmlrpc.php"

@interface MKWordpressEngine()
@property (assign, nonatomic) int blogID;
@property (retain, nonatomic) NSString * username;
@property (retain, nonatomic) NSString * password;
@end

@implementation MKWordpressEngine
@synthesize                 blogID = _blogID;
@synthesize               username = _username;
@synthesize               password = _password;


#pragma mark - Wordpress API
-(void)getCategoriesWithBlogID:(int)blogid 
                  withUserName:(NSString *)username 
                  withPassword:(NSString *)password{
    
    NSMutableArray *params = [[NSMutableArray alloc] init ];
    [params addObject:[NSNumber numberWithInt:blogid]];
    [params addObject:username];
    [params addObject:password];
    
    MKXMLRPCOperation *op = [self operationWithPath:XMLRPC_PATH
                                          callMethod:@"wp.getCategories" 
                                          withParams:params 
                                                 ssl:NO];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        DLog(@"%@", ((MKXMLRPCOperation *)completedOperation).responseXMLRPC);
    } onError:^(NSError *error) {
        DLog(@"error %@", error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getPostsWithBlogID:(int)blogid 
             withUserName:(NSString *)username 
             withPassword:(NSString *)password{
    
    NSMutableArray *params = [[NSMutableArray alloc] init ];
    [params addObject:[NSNumber numberWithInt:blogid]];
    [params addObject:username];
    [params addObject:password];
    
    MKXMLRPCOperation *op = [self operationWithPath:XMLRPC_PATH
                                         callMethod:@"wp.getPosts" 
                                         withParams:params 
                                                ssl:NO];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        DLog(@"%@", ((MKXMLRPCOperation *)completedOperation).responseXMLRPC);
    } onError:^(NSError *error) {
        DLog(@"error %@", error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getPagesWithBlogID:(int)blogid 
             withUserName:(NSString *)username 
             withPassword:(NSString *)password{
    
    NSMutableArray *params = [[NSMutableArray alloc] init ];
    [params addObject:[NSNumber numberWithInt:blogid]];
    [params addObject:username];
    [params addObject:password];
    
    MKXMLRPCOperation *op = [self operationWithPath:XMLRPC_PATH
                                         callMethod:@"wp.getPages" 
                                         withParams:params 
                                                ssl:NO];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        DLog(@"%@", ((MKXMLRPCOperation *)completedOperation).responseXMLRPC);
    } onError:^(NSError *error) {
        DLog(@"error %@", error);
    }];
    
    [self enqueueOperation:op];
}

-(void)newCategoryWithBlogID:(int)blogid 
                    UserName:(NSString *)username 
                    Password:(NSString *)password 
                CategoryName:(NSString *)categoryName 
         CategoryDescription:(NSString *)categoryDescription 
            CategoryParentID:(int)categoryParent_id
                CategorySlug:(NSString *)categorySlug
{
    NSMutableArray *params = [[NSMutableArray alloc] init ];
    [params addObject:[NSNumber numberWithInt:blogid]];
    [params addObject:username];
    [params addObject:password];
    NSMutableDictionary *categoryDetail = [[NSMutableDictionary alloc] init ];
    [categoryDetail setValue:categoryName forKey:@"name"];
    [categoryDetail setValue:categoryDescription forKey:@"description"];
    [categoryDetail setValue:[NSNumber numberWithInt:categoryParent_id] forKey:@"parent_id"];
    [categoryDetail setValue:categorySlug forKey:@"slug"];
    [params addObject:categoryDetail];
    
    MKXMLRPCOperation *op = [self operationWithPath:XMLRPC_PATH
                                          callMethod:@"wp.newCategory" 
                                          withParams:params 
                                                 ssl:NO];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        DLog(@"%@", ((MKXMLRPCOperation *)completedOperation).responseXMLRPC);
    } onError:^(NSError *error) {
        DLog(@"error %@", error);
    }];
    
    [self enqueueOperation:op];  
}

-(void)test{
    [self getPagesWithBlogID:1
                withUserName:USERNAME 
                withPassword:PASSWORD];
}
@end
