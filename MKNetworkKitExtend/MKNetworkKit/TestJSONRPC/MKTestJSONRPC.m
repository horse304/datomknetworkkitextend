//
//  MKTestJSONRPC.m
//  MKNetworkKitExtend
//
//  Created by Techmaster on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKTestJSONRPC.h"

@implementation MKTestJSONRPC
-(void)performAddWithNumberA:(NSNumber *)a 
                 withNumberB:(NSNumber *)b{
    NSMutableArray * params = [[NSMutableArray alloc] init ];
    [params addObject:a];
    [params addObject:b];
    MKJSONRPCOperation *jsonRPCOperation = [self createBatchRequestWithPath:@"jsonrpc/jsonrpcphp/htdocs/server.php"];
    [jsonRPCOperation addRequestForCallMethod:@"add" withParams:params];
    [jsonRPCOperation onCompletion:^(MKNetworkOperation *completedOperation) {
        NSLog(@"responString:%@",completedOperation.responseString);
    } onError:^(NSError *error) {
        ELog(error);
    }];
    [self sendBatchRequest:jsonRPCOperation];
}

-(void)performTruWithNumberA:(NSNumber *)a 
                  withNumberB:(NSNumber *)b{
    NSMutableArray * params = [[NSMutableArray alloc] init ];
    [params addObject:a];
    [params addObject:b];
    MKJSONRPCOperation *jsonRPCOperation = [self createBatchRequestWithPath:@"jsonrpc/jsonrpcphp/htdocs/server.php"];
    [jsonRPCOperation addRequestForCallMethod:@"tru" withParams:params];
    [self sendBatchRequest:jsonRPCOperation];
}

-(void)performMultiWithNumberA:(NSNumber *)a 
                   withNumberB:(NSNumber *)b{
    NSMutableArray * params = [[NSMutableArray alloc] init ];
    [params addObject:a];
    [params addObject:b];
    MKJSONRPCOperation *jsonRPCOperation = [self createBatchRequestWithPath:@"jsonrpc/jsonrpcphp/htdocs/server.php"];
    [jsonRPCOperation addRequestForCallMethod:@"add" withParams:params];
    [jsonRPCOperation addRequestForCallMethod:@"tru" withParams:params];
    [jsonRPCOperation addRequestForCallMethod:@"add" withParams:params];
    [self sendBatchRequest:jsonRPCOperation];
}

-(void)test{
    [self performTruWithNumberA:[NSNumber numberWithInt:2] 
                    withNumberB:[NSNumber numberWithInt:3]];
}
@end
