//
//  MKTestJSONRPC.h
//  MKNetworkKitExtend
//
//  Created by Techmaster on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
// Test

#import "MKJSONRPCEngine.h"

@interface MKTestJSONRPC : MKJSONRPCEngine
-(void)test;
@end
