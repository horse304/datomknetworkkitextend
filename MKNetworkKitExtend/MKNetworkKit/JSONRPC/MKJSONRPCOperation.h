//
//  MKJSONRPCOperation.h
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

@class MKJSONRPCOperation;

@interface MKJSONRPCOperation : MKNetworkOperation

@property (strong, nonatomic) id responseJSONRPC;

- (id) initBatchRequestWithURLString:(NSString *)aURLString;

-(NSString *) addRequestForCallMethod:(NSString *)methodName
                           withParams:(NSMutableArray *)params;

-(NSString *) addNotificationForCallMethod:(NSString *)methodName 
                                withParams:(NSMutableArray *)params;

-(BOOL) removeRequestOrNotificationWithIdentifier:(NSString *)identifier;

-(BOOL) editRequestOrNotificationWithIdentifier:(NSString *)identifier
                                     callMethod:(NSString *)methodName
                                     withParams:(NSMutableArray *)params;
@end
