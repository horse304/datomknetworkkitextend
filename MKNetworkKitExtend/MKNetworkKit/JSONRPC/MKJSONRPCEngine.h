//
//  MKJSONRPCEngine.h
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

@interface MKJSONRPCEngine : MKNetworkEngine
/*!
 *  @abstract Post 1 request theo chuan JSON RPC toi server
 *  
 *  @discussion
 *	Tao 1 operation voi duong dan URL truyen vao
 *  Convert methodName va params truyen vao -> theo chuan JSON RPC de gui len server.
 *  HTTP Method mac dinh la post
 *  Text encoding mac dinh la application/json
 *  
 */
-(MKJSONRPCOperation *) createBatchRequestWithPath:(NSString *)path;

-(MKJSONRPCOperation *) createBatchRequestWithPath:(NSString *)path 
                                               ssl:(BOOL)useSSL;

-(void) sendBatchRequest:(MKJSONRPCOperation *)jsonRPCOperation;
@end
