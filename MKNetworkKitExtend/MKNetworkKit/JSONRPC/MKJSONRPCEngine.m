//
//  MKJSONRPCEngine.m
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKJSONRPCEngine.h"

@implementation MKJSONRPCEngine

-(MKJSONRPCOperation *)createBatchRequestWithPath:(NSString *)path{
    return [self createBatchRequestWithPath:path
                                        ssl:NO];
}

-(MKJSONRPCOperation *)createBatchRequestWithPath:(NSString *)path 
                                              ssl:(BOOL)useSSL{
    if(self.readonlyHostName == nil) {
        DLog(@"Hostname is nil, use operationWithURLString: method to create absolute URL operations");
        return nil;
    }
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@://%@", useSSL ? @"https" : @"http", self.readonlyHostName];
    
    if(self.portNumber != 0)
        [urlString appendFormat:@":%d", self.portNumber];
    
    if(self.apiPath) 
        [urlString appendFormat:@"/%@", self.apiPath];
    
    [urlString appendFormat:@"/%@", path];
    
    return [[MKJSONRPCOperation alloc] initBatchRequestWithURLString:urlString];
}

-(void)sendBatchRequest:(MKJSONRPCOperation *)jsonRPCOperation{
    [self enqueueOperation:jsonRPCOperation];
}

@end
