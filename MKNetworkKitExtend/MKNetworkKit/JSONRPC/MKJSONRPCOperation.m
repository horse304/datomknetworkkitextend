//
//  MKJSONRPCOperation.m
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/3/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKJSONRPCOperation.h"

#define JSONRPC_VERSION @"2.0"
#define REQUEST_PARAMS_JSONRPC_KEY @"jsonrpc"
#define REQUEST_PARAMS_IDENTIFIER_KEY @"identifier"
#define REQUEST_PARAMS_METHOD_KEY @"method"
#define REQUEST_PARAMS_PARAMS_KEY @"params"
#define REQUEST_PARAMS_REQUESTID_KEY @"id"
#define JSONRPC_TYPE_ENCODING @"application/json"

@interface MKJSONRPCOperation()
@property (strong, nonatomic) NSMutableArray *requestManager;
@end

@implementation MKJSONRPCOperation
@synthesize                  requestManager = _requestManager;
@synthesize                 responseJSONRPC = _responseJSONRPC;


-(id)initBatchRequestWithURLString:(NSString *)aURLString{
    self = [super initWithURLString:aURLString 
                             params:nil 
                         httpMethod:@"POST"];
    if (self) {
        //Init request manager
        self.requestManager = [[NSMutableArray alloc] init];
        
        //Set encode handler
        __block MKJSONRPCOperation *jsonRPCOperation = self;
        [self setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
            return [jsonRPCOperation encode:jsonRPCOperation.requestManager];
        } forType:JSONRPC_TYPE_ENCODING];
        
        //Set decode handler
        [self onCompletion:^(MKNetworkOperation *completedOperation) {
            MKJSONRPCOperation *jsonOperation = (MKJSONRPCOperation *)completedOperation;
            id decodedOperation = [jsonOperation decode:jsonOperation.responseData];
            [jsonOperation setResponseJSONRPC:decodedOperation];
        } onError:^(NSError *error) {
            ELog(error);
        } ];
    }
    
    return self;
}

#pragma mark - JSONRPC Encode funtions
- (NSString *)encode:(NSMutableArray *)requestManager{
    NSString *encodedString = @"";
    if (requestManager.count > 1) {
        encodedString = [requestManager JSONString];
    }else if(requestManager.count == 1) {
        encodedString = [[requestManager objectAtIndex:0] JSONString];
    }else {
        encodedString = @"";
    }
    DLog(@"json encoded:%@",encodedString);
    return encodedString;
}

#pragma mark - JSONRPC Decode funtions
- (id)decode:(NSData *)responseData{
    id decodedObject = [responseData objectFromJSONData];
    DLog(@"json decoded:%@",decodedObject);
    return decodedObject;
}

#pragma mark - Add, remove, edit JSON-RPC Requests
-(NSMutableDictionary *)createRequestOrNotificationForMethod:(NSString *)methodName
                                                  withParams:(NSMutableArray *)params
                                              isNotification:(BOOL)isNotification{
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init ];
    [requestDict setValue:JSONRPC_VERSION forKey:REQUEST_PARAMS_JSONRPC_KEY];
    [requestDict setValue:methodName forKey:REQUEST_PARAMS_METHOD_KEY];
    [requestDict setValue:params forKey:REQUEST_PARAMS_PARAMS_KEY];
    NSString *identifier = CREATE_UUID;
    if (!isNotification) {
        [requestDict setValue:identifier forKey:REQUEST_PARAMS_REQUESTID_KEY];
    }
    [requestDict setValue:identifier forKey:REQUEST_PARAMS_IDENTIFIER_KEY];
    
    return requestDict;
}


-(NSString *)addRequestOrNotificationForCallMethod:(NSString *)methodName
                                        withParams:(NSMutableArray *)params 
                                    isNotification:(BOOL)isNotification{
    NSMutableDictionary *requestItem = [self createRequestOrNotificationForMethod:methodName 
                                                                       withParams:params 
                                                                   isNotification:isNotification];
    NSString *identifier = [requestItem valueForKey:REQUEST_PARAMS_IDENTIFIER_KEY];
    [self.requestManager addObject:requestItem];
    return identifier;
}

-(NSString *)addRequestForCallMethod:(NSString *)methodName 
                          withParams:(NSMutableArray *)params{
    return [self addRequestOrNotificationForCallMethod:methodName 
                                            withParams:params 
                                        isNotification:NO];
}

-(NSString *)addNotificationForCallMethod:(NSString *)methodName 
                               withParams:(NSMutableArray *)params{
    return [self addRequestOrNotificationForCallMethod:methodName 
                                            withParams:params 
                                        isNotification:YES];
}

-(BOOL)removeRequestOrNotificationWithIdentifier:(NSString *)identifier{
    for (NSMutableDictionary *requestItem in self.requestManager) {
        if ([[requestItem valueForKey:REQUEST_PARAMS_IDENTIFIER_KEY] isEqualToString:identifier]) {
            [self.requestManager removeObject:requestItem];
            return TRUE;
        }
    }
    
    return FALSE;
}

-(BOOL)editRequestOrNotificationWithIdentifier:(NSString *)identifier 
                                    callMethod:(NSString *)methodName 
                                    withParams:(NSMutableArray *)params{
    for (NSMutableDictionary *requestItem in self.requestManager) {
        if ([[requestItem valueForKey:REQUEST_PARAMS_IDENTIFIER_KEY] isEqualToString:identifier]) {
            [requestItem setValue:methodName forKey:REQUEST_PARAMS_METHOD_KEY];
            [requestItem setValue:params forKey:REQUEST_PARAMS_PARAMS_KEY];
            return TRUE;
        }
    }
    
    return FALSE;
}

@end
