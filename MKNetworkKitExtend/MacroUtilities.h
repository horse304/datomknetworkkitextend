//
//  MacroUtilities.h
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#ifndef CREATE_UUID
#   define CREATE_UUID ((__bridge NSString *)CFUUIDCreateString(NULL, CFUUIDCreate(NULL)))
#endif

#ifdef DEBUG
#   ifndef DLog
#       define DLog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#   endif
#   ifndef ELog
#       define ELog(err) {if(err) DLog(@"%@", err)}
#   endif
#else
#   ifndef DLog
#       define DLog(...)
#   endif
#   ifndef ELog
#       define ELog(err)
#   endif
#endif
