//
//  ViewController.m
//  TestXMLRPCWordPress
//
//  Created by Dato - horseuvn@gmail.com on 8/31/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "ViewController.h"
#import "MKWordpressEngine.h"
#import "MKTestJSONRPC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //MKWordpressEngine *wordpress = [[MKWordpressEngine alloc] initWithHostName:@"atwteam.com"];
    //[wordpress test];
    MKTestJSONRPC *testJSON = [[MKTestJSONRPC alloc] initWithHostName:@"atwteam.com"];
    [testJSON test];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
